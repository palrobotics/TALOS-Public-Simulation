# Dockers

This repository contains one or more docker images

## What is a docker?

Docker is a platform for developers and sysadmins to develop, deploy, and run applications with containers. The use of Linux containers to deploy applications is called containerization. Containers are not new, but their use for easily deploying applications is.

Containerization is increasingly popular because containers are:

- Flexible: Even the most complex applications can be containerized.
- Lightweight: Containers leverage and share the host kernel.
- Interchangeable: You can deploy updates and upgrades on-the-fly.
- Portable: You can build locally, deploy to the cloud, and run anywhere.
- Scalable: You can increase and automatically distribute container replicas.
- Stackable: You can stack services vertically and on-the-fly.

If you are interested on how docker works, visit her [webpage](https://docs.docker.com/)

## Where are the docker images located?

On gitlab.com, your images should be inside the *Container Registry* of this project. This is accessible on the left sidebar, sometimes under the *Packages* entry.

In there you will see all the images provided to you, you can use the **copy** button to retrieve the url for using it, it will be needed later instead of PATH\_TO\_YOUR\_DOCKER\_IMAGE

## Run a docker

In order to start any of the dockers loaded on this repository, you will need to log in the docker daemon on gitlab.com:

```
$ docker login registry.gitlab.com
```
an then enter your personal GitLab user and password.

For your convenience, we have published some scripts that simplify the launch of a docker with GPU acceleration. Follow the instructions at [pal_docker_utils](https://github.com/pal-robotics/pal_docker_utils/) to properly set up your environment with nvidia-docker. If you do not follow the steps properly, you will not be able to run gazebo, rviz or other graphical applications from within the docker container.

Once logged and after configuring pal\_docker\_utils, you will need to execute the [pal_docker.sh](https://github.com/pal-robotics/pal_docker_utils/blob/master/scripts/pal_docker.sh) script with the name of the image and the application you want to start.
```
$ ./pal_docker.sh -it PATH_TO_YOUR_DOCKER_IMAGE bash
```
The previous command starts a bash terminal inside a container of the specified image.


**Attention!** Remember that once inside the docker, any file created or any program installed on the docker will be deleted, if the specific file was not saved on the exchange folder. If the user needs to install a speicifc software everytime, it is better to create a new docker following the instructions on the [tutorials](https://docs.docker.com/get-started/), and taking as a base the desired docker.
 image.
